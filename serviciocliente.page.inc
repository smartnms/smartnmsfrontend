<?php

/**
 * @file
 * Contains serviciocliente.page.inc.
 *
 * Page callback for Serviciocliente entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Serviciocliente templates.
 *
 * Default template: serviciocliente.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_serviciocliente(array &$variables) {
  // Fetch serviciocliente Entity Object.
  $serviciocliente = $variables['elements']['#serviciocliente'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
