<?php
/**
 * Viewbuilder for servicio cliente
 * Date: 08/12/2017
 * Time: 13:15
 * v 0.1
 */

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Url;
use Drupal\smartnmsfrontend\Entity\serviciocliente;


/**
 * Render controller for serviciocliente.
 */

class servicioclienteViewBuilder extends EntityViewBuilder {


    public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {

        if (empty($entities)) {
            return;
        }

        parent::buildComponents($build, $entities, $displays, $view_mode);

        $build['#attached']['library'][] = 'zabbixentities/smartnms';
        foreach ($entities as $id => $entity) {
            // We put the display components on a different detail
            $build[$id]['fields']=array('#type'=>'details',
                '#title' => $this->t('Service Details'),
                '#description' => t('Custom values for Service'),
                '#open' => FALSE,);
            foreach($build[$id] as $name => $attribute) {
                if ($entity->hasField($name)) {
                    $build[$id]['fields'][$name] = $attribute;
                    unset($build[$id][$name]);
                }
            }
            // Add Status to render array.
            $build[$id]['status']=array('#type'=>'details',
                '#title' => $this->t('Status'),
                '#description' => t('Status summary of hosts in the service'),
                '#open' => TRUE,);
            $hosts=$entity->getHosts();
            foreach($hosts as $host){
                $entity_ids[]=$host->id();
            }
            if(count($entity_ids)>0) {
                $listbuilder = \Drupal::entityTypeManager()->getListBuilder('zabbixhost');
                $listbuilder->entity_ids = $entity_ids;
                $listbuilder->usefilter = TRUE;
                $build[$id]['status']['componentes'] = $listbuilder->render();
            }
            else{
                $build[$id]['status']['componentes'] = array('#markup'=>t('There are no servers on this service'));
            }

        }
    }
}
