<?php

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Serviciocliente entities.
 *
 * @ingroup smartnmsfrontend
 */
class servicioclienteListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Serviciocliente ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\smartnmsfrontend\Entity\serviciocliente */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.serviciocliente.edit_form', [
          'serviciocliente' => $entity->id(),
        ]
      )
    );
    return $row + parent::buildRow($entity);
  }

}
