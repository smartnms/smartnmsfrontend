<?php

namespace Drupal\smartnmsfrontend\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class moduloplataformaTypeForm.
 *
 * @package Drupal\smartnmsfrontend\Form
 */
class moduloplataformaTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $moduloplataforma_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $moduloplataforma_type->label(),
      '#description' => $this->t("Label for the Moduloplataforma type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $moduloplataforma_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\smartnmsfrontend\Entity\moduloplataformaType::load',
      ],
      '#disabled' => !$moduloplataforma_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $moduloplataforma_type = $this->entity;
    $status = $moduloplataforma_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Moduloplataforma type.', [
          '%label' => $moduloplataforma_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Moduloplataforma type.', [
          '%label' => $moduloplataforma_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($moduloplataforma_type->toUrl('collection'));
  }

}
