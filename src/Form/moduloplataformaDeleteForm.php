<?php

namespace Drupal\smartnmsfrontend\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Moduloplataforma entities.
 *
 * @ingroup smartnmsfrontend
 */
class moduloplataformaDeleteForm extends ContentEntityDeleteForm {


}
