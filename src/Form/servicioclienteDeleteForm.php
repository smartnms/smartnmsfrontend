<?php

namespace Drupal\smartnmsfrontend\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Serviciocliente entities.
 *
 * @ingroup smartnmsfrontend
 */
class servicioclienteDeleteForm extends ContentEntityDeleteForm {


}
