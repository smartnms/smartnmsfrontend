<?php

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\smartnmsfrontend\Entity\servicioclienteInterface;

/**
 * Defines the storage handler class for Serviciocliente entities.
 *
 * This extends the base storage class, adding required special handling for
 * Serviciocliente entities.
 *
 * @ingroup smartnmsfrontend
 */
interface servicioclienteStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Serviciocliente revision IDs for a specific Serviciocliente.
   *
   * @param \Drupal\smartnmsfrontend\Entity\servicioclienteInterface $entity
   *   The Serviciocliente entity.
   *
   * @return int[]
   *   Serviciocliente revision IDs (in ascending order).
   */
  public function revisionIds(servicioclienteInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Serviciocliente author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Serviciocliente revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\smartnmsfrontend\Entity\servicioclienteInterface $entity
   *   The Serviciocliente entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(servicioclienteInterface $entity);

  /**
   * Unsets the language for all Serviciocliente with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
