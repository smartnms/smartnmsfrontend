<?php

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\smartnmsfrontend\Entity\servicioclienteInterface;

/**
 * Defines the storage handler class for Serviciocliente entities.
 *
 * This extends the base storage class, adding required special handling for
 * Serviciocliente entities.
 *
 * @ingroup smartnmsfrontend
 */
class servicioclienteStorage extends SqlContentEntityStorage implements servicioclienteStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(servicioclienteInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {serviciocliente_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {serviciocliente_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(servicioclienteInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {serviciocliente_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('serviciocliente_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
