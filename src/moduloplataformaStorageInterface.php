<?php

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\smartnmsfrontend\Entity\moduloplataformaInterface;

/**
 * Defines the storage handler class for Moduloplataforma entities.
 *
 * This extends the base storage class, adding required special handling for
 * Moduloplataforma entities.
 *
 * @ingroup smartnmsfrontend
 */
interface moduloplataformaStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Moduloplataforma revision IDs for a specific Moduloplataforma.
   *
   * @param \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface $entity
   *   The Moduloplataforma entity.
   *
   * @return int[]
   *   Moduloplataforma revision IDs (in ascending order).
   */
  public function revisionIds(moduloplataformaInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Moduloplataforma author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Moduloplataforma revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface $entity
   *   The Moduloplataforma entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(moduloplataformaInterface $entity);

  /**
   * Unsets the language for all Moduloplataforma with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
