<?php

namespace Drupal\smartnmsfrontend;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for serviciocliente.
 */
class servicioclienteTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
