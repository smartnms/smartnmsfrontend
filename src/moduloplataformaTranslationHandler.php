<?php

namespace Drupal\smartnmsfrontend;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for moduloplataforma.
 */
class moduloplataformaTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
