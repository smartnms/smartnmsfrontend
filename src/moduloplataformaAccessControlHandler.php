<?php

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Moduloplataforma entity.
 *
 * @see \Drupal\smartnmsfrontend\Entity\moduloplataforma.
 */
class moduloplataformaAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished moduloplataforma entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published moduloplataforma entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit moduloplataforma entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete moduloplataforma entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add moduloplataforma entities');
  }

}
