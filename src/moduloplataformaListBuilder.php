<?php

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Moduloplataforma entities.
 *
 * @ingroup smartnmsfrontend
 */
class moduloplataformaListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Moduloplataforma ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\smartnmsfrontend\Entity\moduloplataforma */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.moduloplataforma.edit_form', [
          'moduloplataforma' => $entity->id(),
        ]
      )
    );
    return $row + parent::buildRow($entity);
  }

}
