<?php
/**
 * @file
 * Contains \Drupal\smartnmsfront\Plugin\Block\front_block.
 */
namespace Drupal\smartnmsfrontend\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\smartnmsfrontend\Entity\serviciocliente;

/**
 * Provides a 'front_block' block.
 *
 * @Block(
 *   id = "servicestatus_block",
 *   admin_label = "SmartNMS Services block",
 *   category = "Blocks"
 * )
 */
class servicestatus_block extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
      $entities=ServicioCliente::loadMultiple();
      $rows=array();
      foreach($entities as $entity){
          $hosts=$entity->getHosts();
          $name=$entity->getName();
          $trigger_status=0;
          $zbx_status=0;
          $snmp_status=0;
          $ipmi_status=0;
          $jmx_status=0;
          $zbx_errors=array();
          $snmp_errors=array();
          $ipmi_errors=array();
          $jmx_errors=array();
          $trigger_errors=array();
          foreach($hosts as $host){
              $status=$host->serverStatus();
              if($status['zbx']['value']>=$zbx_status) {
                  $zbx_status = $status['zbx']['value'];
                  $zbx_errors[]=$status['zbx']['error'];
              }
              if($status['snmp']['value']>=$snmp_status) {
                  $snmp_status = $status['snmp']['value'];
                  $snmp_errors[]=$status['snmp']['error'];
              }
              if($status['ipmi']['value']>=$ipmi_status) {
                  $ipmi_status = $status['ipmi']['value'];
                  $ipmi_errors[]=$status['ipmi']['error'];
              }
              if($status['jmx']['value']>=$jmx_status) {
                  $jmx_status = $status['jmx']['value'];
                  $jmx_errors[]=$status['jmx']['error'];
              }
              foreach($status['triggers'] as $trigger){
                  $trigger_errors[]=str_replace('{HOST.NAME}',$host->getName(),$trigger['description']);
                  if($trigger['priority']>$trigger_status) {
                      $trigger_status=$trigger['priority'];
                  }
              }
          }
          $errortext='';
          $row_class="circleok";
          if($zbx_status==1)
              $row_class="circleok";
          elseif($zbx_status==2){
              $row_class="circleerror";
              foreach($zbx_errors as $error){
                if(strlen($error)>1)
                    $errortext.=$error."\n";
              }
          }
          if($snmp_status==1)
                break;
          elseif($snmp_status==2){
              $row_class="circleerror";
              foreach($snmp_errors as $error){
                  if(strlen($error)>1)
                      $errortext.=$error."\n";
              }
          }
          if($ipmi_status==1)
              break;
          elseif($ipmi_status==2) {
              $row_class="circleerror";
              foreach ($ipmi_errors as $error) {
                  if (strlen($error) > 1)
                      $errortext .= $error . "\n";
              }
          }
          if($jmx_status==1)
              break;
          elseif($jmx_status==2) {
              $row_class="circleerror";
              foreach ($jmx_errors as $error) {
                  if (strlen($error) > 1)
                      $errortext .= $error . "\n";
              }
          }
          if($row_class=="circleok") {
              switch ($trigger_status) {
                  case '1':
                      $row_class="circletrigger_1";
                      break;
                  case '2':
                      $row_class="circletrigger_2";
                      break;
                  case '3':
                      $row_class="circletrigger_3";
                      break;
                  case '4':
                      $row_class="circletrigger_4";
                      break;
                  case '5':
                      $row_class="circletrigger_5";
                      break;
                  default:
                      $row_class="circletrigger_3";
                      break;
              }
          }
          foreach($trigger_errors as $error){
              if(strlen($error)>1)
                  $errortext.=$error."\n";
          }
          $options=array('attributes'=>array('class'=>'blockmiddle'));
          $row_value='<span class="'.$row_class.'"> </span>';
          $row=array('data'=>array('name'=> array('data'=>\Drupal\Core\Link::createFromRoute($name, 'smartnmsfrontend.customer',
                                    ['customerservice' => $entity->id()],$options),
                                    'class' => array('blockmiddle'),),
                                    'status'=>new FormattableMarkup($row_value,array())),
                                'title'=>$errortext,
                      'class' => array('serviceblock'),
          );
          $rows[]=$row;
      }
      $output=array();
      $output[]['#cache']['max-age'] = 300; // 5 min cache
      $output[]=array(
          '#theme' => 'servicestatus',
          '#content' => array(
          '#type' =>'table',
          '#title' => t('Service Status'),
          '#header'=>array(t('Service'),t('Status')),
          '#rows' => $rows),
      );
      $output['#attached']['library'][] = 'zabbixentities/smartnms';
      return $output;
  }
}
?>