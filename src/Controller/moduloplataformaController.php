<?php

namespace Drupal\smartnmsfrontend\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\smartnmsfrontend\Entity\moduloplataformaInterface;

/**
 * Class moduloplataformaController.
 *
 *  Returns responses for Moduloplataforma routes.
 *
 * @package Drupal\smartnmsfrontend\Controller
 */
class moduloplataformaController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Moduloplataforma  revision.
   *
   * @param int $moduloplataforma_revision
   *   The Moduloplataforma  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($moduloplataforma_revision) {
    $moduloplataforma = $this->entityManager()->getStorage('moduloplataforma')->loadRevision($moduloplataforma_revision);
    $view_builder = $this->entityManager()->getViewBuilder('moduloplataforma');

    return $view_builder->view($moduloplataforma);
  }

  /**
   * Page title callback for a Moduloplataforma  revision.
   *
   * @param int $moduloplataforma_revision
   *   The Moduloplataforma  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($moduloplataforma_revision) {
    $moduloplataforma = $this->entityManager()->getStorage('moduloplataforma')->loadRevision($moduloplataforma_revision);
    return $this->t('Revision of %title from %date', ['%title' => $moduloplataforma->label(), '%date' => format_date($moduloplataforma->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Moduloplataforma .
   *
   * @param \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface $moduloplataforma
   *   A Moduloplataforma  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(moduloplataformaInterface $moduloplataforma) {
    $account = $this->currentUser();
    $langcode = $moduloplataforma->language()->getId();
    $langname = $moduloplataforma->language()->getName();
    $languages = $moduloplataforma->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $moduloplataforma_storage = $this->entityManager()->getStorage('moduloplataforma');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $moduloplataforma->label()]) : $this->t('Revisions for %title', ['%title' => $moduloplataforma->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all moduloplataforma revisions") || $account->hasPermission('administer moduloplataforma entities')));
    $delete_permission = (($account->hasPermission("delete all moduloplataforma revisions") || $account->hasPermission('administer moduloplataforma entities')));

    $rows = [];

    $vids = $moduloplataforma_storage->revisionIds($moduloplataforma);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\smartnmsfrontend\moduloplataformaInterface $revision */
      $revision = $moduloplataforma_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $moduloplataforma->getRevisionId()) {
          $link = $this->l($date, new Url('entity.moduloplataforma.revision', ['moduloplataforma' => $moduloplataforma->id(), 'moduloplataforma_revision' => $vid]));
        }
        else {
          $link = $moduloplataforma->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.moduloplataforma.translation_revert', ['moduloplataforma' => $moduloplataforma->id(), 'moduloplataforma_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.moduloplataforma.revision_revert', ['moduloplataforma' => $moduloplataforma->id(), 'moduloplataforma_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.moduloplataforma.revision_delete', ['moduloplataforma' => $moduloplataforma->id(), 'moduloplataforma_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['moduloplataforma_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
