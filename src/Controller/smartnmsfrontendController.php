<?php
/**
 * @file
 * Contains \Drupal\example\Controller\ExampleController.
 */
namespace Drupal\smartnmsfrontend\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Link;
use Drupal\smartnmsfrontend\Entity\moduloplataforma;


class smartnmsfrontendController {


	public function startAction(){
        $output['#cache']['max-age'] = 300; // 5 min cache
        $output['#theme']='modulesoverview';
        return $output;
	}

	public function customerStatus($customerservice)
    {
        $output = array();
        $output['#cache']['max-age'] = 120; // 5 min cache
        $output['#theme'] = 'modulesoverview';
        $output['#attached']['library'][] = 'zabbixentities/smartnms';
        $service = \Drupal::entityTypeManager()->getStorage('serviciocliente')->load($customerservice);
        $servicedhosts = $service->getHosts();
        $entities=ModuloPlataforma::loadMultiple();
        foreach ($entities as $entity) {
            $hosts = $entity->getHosts();
            $name = $entity->getName();
            $sanitized_name = strtolower(strtr($name, array('á' => 'a', 'é' => 'e', 'í' => 'i', 'ó' => 'o')));
            $boxclass = 'cajaneutra';
            $trigger_status = 0;
            foreach ($hosts as $host) {
                $exists = FALSE;
                foreach ($servicedhosts as $serviced) {
                    if ($serviced->getHostid() == $host->getHostid()) {
                        $exists = TRUE;
                        break;
                    }
                }
                if (!$exists)
                    continue;
                $status = $host->serverStatus();
                $zbx_status = $status['zbx']['value'];
                $snmp_status = $status['snmp']['value'];
                $ipmi_status = $status['ipmi']['value'];
                $jmx_status = $status['jmx']['value'];
                foreach ($status['triggers'] as $trigger) {
                    if ($trigger['priority'] > $trigger_status) {
                        $trigger_status = $trigger['priority'];
                    }
                }
                if ($zbx_status == 1 && $boxclass == 'cajaneutra') {
                    $boxclass = 'cajaok';
                } elseif ($zbx_status == 2 || $snmp_status == 2 || $ipmi_status == 2 || $jmx_status == 2) {
                    $boxclass = 'cajaerror';
                }
                if($boxclass=='cajaok') {
                    switch ($trigger_status) {
                        case '1':
                            $boxclass = "cajatrigger_1";
                            break;
                        case '2':
                            $boxclass = "cajatrigger_2";
                            break;
                        case '3':
                            $boxclass = "cajatrigger_3";
                            break;
                        case '4':
                            $boxclass = "cajatrigger_4";
                            break;
                        case '5':
                            $boxclass = "cajaerror";
                            break;
                        default:
                            break;
                    }
                }
            }
        $params=array();
        $options=array();
        $params['platformmodule']=$entity->id();
        $params['customerservice']=$customerservice;
        $ln=Link::CreateFromRoute('Enlace','smartnmsfrontend.customerserviceoverview',$params,$options);
        $output[$sanitized_name]['clase'] = $boxclass;
        $output[$sanitized_name]['link']= $ln->getUrl()->toString();
        }
        return $output;
    }

	public function customerModuleOverview($customerservice,$platformmodule)
    {
        $output = array();
        $output['#cache']['max-age'] = 120; // 5 min cache
//        $output['#theme'] = 'modulesoverview';
        $output['#attached']['library'][] = 'zabbixentities/smartnms';
        $service = \Drupal::entityTypeManager()->getStorage('serviciocliente')->load($customerservice);
        $module = \Drupal::entityTypeManager()->getStorage('moduloplataforma')->load($platformmodule);
        $servicedhosts = $service->getHosts();
        $inmodulehosts = $module->getHosts();
        $shosts=array();
        $mhosts=array();
        foreach($servicedhosts as $entity)
            $shosts[]=$entity->id();
        foreach($inmodulehosts as $entity)
            $mhosts[]=$entity->id();
        $hosts=array_intersect($shosts,$mhosts);
        // Add Status to render array.
        $output['status']=array('#type'=>'details',
            '#title' => t('Status'),
            '#description' => t('Status summary of hosts in the service'),
            '#open' => TRUE,);
        if(count($hosts)>0) {
            $listbuilder = \Drupal::entityTypeManager()->getListBuilder('zabbixhost');
            $listbuilder->entity_ids = $hosts;
            $listbuilder->usefilter = TRUE;
            $output['status']['componentes'] = $listbuilder->render();
        }
        else{
            $output['status']['componentes'] = array('#markup'=>t('There are no servers on this module for this service'));
        }
        return $output;
    }
}
?>
