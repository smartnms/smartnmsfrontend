<?php

namespace Drupal\smartnmsfrontend\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\smartnmsfrontend\Entity\servicioclienteInterface;

/**
 * Class servicioclienteController.
 *
 *  Returns responses for Serviciocliente routes.
 *
 * @package Drupal\smartnmsfrontend\Controller
 */
class servicioclienteController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Serviciocliente  revision.
   *
   * @param int $serviciocliente_revision
   *   The Serviciocliente  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($serviciocliente_revision) {
    $serviciocliente = $this->entityManager()->getStorage('serviciocliente')->loadRevision($serviciocliente_revision);
    $view_builder = $this->entityManager()->getViewBuilder('serviciocliente');

    return $view_builder->view($serviciocliente);
  }

  /**
   * Page title callback for a Serviciocliente  revision.
   *
   * @param int $serviciocliente_revision
   *   The Serviciocliente  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($serviciocliente_revision) {
    $serviciocliente = $this->entityManager()->getStorage('serviciocliente')->loadRevision($serviciocliente_revision);
    return $this->t('Revision of %title from %date', ['%title' => $serviciocliente->label(), '%date' => format_date($serviciocliente->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Serviciocliente .
   *
   * @param \Drupal\smartnmsfrontend\Entity\servicioclienteInterface $serviciocliente
   *   A Serviciocliente  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(servicioclienteInterface $serviciocliente) {
    $account = $this->currentUser();
    $langcode = $serviciocliente->language()->getId();
    $langname = $serviciocliente->language()->getName();
    $languages = $serviciocliente->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $serviciocliente_storage = $this->entityManager()->getStorage('serviciocliente');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $serviciocliente->label()]) : $this->t('Revisions for %title', ['%title' => $serviciocliente->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all serviciocliente revisions") || $account->hasPermission('administer serviciocliente entities')));
    $delete_permission = (($account->hasPermission("delete all serviciocliente revisions") || $account->hasPermission('administer serviciocliente entities')));

    $rows = [];

    $vids = $serviciocliente_storage->revisionIds($serviciocliente);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\smartnmsfrontend\servicioclienteInterface $revision */
      $revision = $serviciocliente_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $serviciocliente->getRevisionId()) {
          $link = $this->l($date, new Url('entity.serviciocliente.revision', ['serviciocliente' => $serviciocliente->id(), 'serviciocliente_revision' => $vid]));
        }
        else {
          $link = $serviciocliente->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.serviciocliente.translation_revert', ['serviciocliente' => $serviciocliente->id(), 'serviciocliente_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.serviciocliente.revision_revert', ['serviciocliente' => $serviciocliente->id(), 'serviciocliente_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.serviciocliente.revision_delete', ['serviciocliente' => $serviciocliente->id(), 'serviciocliente_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['serviciocliente_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
