<?php

namespace Drupal\smartnmsfrontend;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Serviciocliente entity.
 *
 * @see \Drupal\smartnmsfrontend\Entity\serviciocliente.
 */
class servicioclienteAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\smartnmsfrontend\Entity\servicioclienteInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished serviciocliente entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published serviciocliente entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit serviciocliente entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete serviciocliente entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add serviciocliente entities');
  }

}
