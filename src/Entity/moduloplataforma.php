<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Moduloplataforma entity.
 *
 * @ingroup smartnmsfrontend
 *
 * @ContentEntityType(
 *   id = "moduloplataforma",
 *   label = @Translation("Platform Module"),
 *   bundle_label = @Translation("Type of Platform Module"),
 *   handlers = {
 *     "storage" = "Drupal\smartnmsfrontend\moduloplataformaStorage",
 *     "view_builder" = "Drupal\smartnmsfrontend\moduloplataformaViewBuilder",
 *     "list_builder" = "Drupal\smartnmsfrontend\moduloplataformaListBuilder",
 *     "views_data" = "Drupal\smartnmsfrontend\Entity\moduloplataformaViewsData",
 *     "translation" = "Drupal\smartnmsfrontend\moduloplataformaTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\smartnmsfrontend\Form\moduloplataformaForm",
 *       "add" = "Drupal\smartnmsfrontend\Form\moduloplataformaForm",
 *       "edit" = "Drupal\smartnmsfrontend\Form\moduloplataformaForm",
 *       "delete" = "Drupal\smartnmsfrontend\Form\moduloplataformaDeleteForm",
 *     },
 *     "access" = "Drupal\smartnmsfrontend\moduloplataformaAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\smartnmsfrontend\moduloplataformaHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "moduloplataforma",
 *   data_table = "moduloplataforma_field_data",
 *   revision_table = "moduloplataforma_revision",
 *   revision_data_table = "moduloplataforma_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer moduloplataforma entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/smart/moduloplataforma/{moduloplataforma}",
 *     "add-page" = "/smart/moduloplataforma/add",
 *     "add-form" = "/smart/moduloplataforma/add/{moduloplataforma_type}",
 *     "edit-form" = "/smart/moduloplataforma/{moduloplataforma}/edit",
 *     "delete-form" = "/smart/moduloplataforma/{moduloplataforma}/delete",
 *     "version-history" = "/smart/moduloplataforma/{moduloplataforma}/revisions",
 *     "revision" = "/smart/moduloplataforma/{moduloplataforma}/revisions/{moduloplataforma_revision}/view",
 *     "revision_revert" = "/smart/moduloplataforma/{moduloplataforma}/revisions/{moduloplataforma_revision}/revert",
 *     "translation_revert" = "/smart/moduloplataforma/{moduloplataforma}/revisions/{moduloplataforma_revision}/revert/{langcode}",
 *     "revision_delete" = "/smart/moduloplataforma/{moduloplataforma}/revisions/{moduloplataforma_revision}/delete",
 *     "collection" = "/smart/moduloplataforma",
 *   },
 *   bundle_entity_type = "moduloplataforma_type",
 *   field_ui_base_route = "entity.moduloplataforma_type.edit_form"
 * )
 */
class moduloplataforma extends RevisionableContentEntityBase implements moduloplataformaInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the moduloplataforma owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

    /**
     * {@inheritdoc}
     */
    public function getHosts() {
        return $this->get('host_id')->referencedEntities();
    }

    /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Module.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Module.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['description'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Description'))
          ->setDescription(t('The description of the Module.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 50,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => -4,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => -4,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['host_id'] = BaseFieldDefinition::create('entity_reference')
          ->setLabel(t('Host'))
          ->setDescription(t('Hosts that are part of the module.'))
          ->setRevisionable(TRUE)
          ->setSetting('target_type', 'zabbixhost')
          ->setSetting('handler', 'default')
          ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
          ->setTranslatable(TRUE)
          ->setDisplayOptions('view', [
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'entity_reference_autocomplete',
              'weight' => 5,
              'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => '60',
                  'placeholder' => '',
              ],
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Moduloplataforma is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
