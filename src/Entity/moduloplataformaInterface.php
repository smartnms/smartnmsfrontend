<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Moduloplataforma entities.
 *
 * @ingroup smartnmsfrontend
 */
interface moduloplataformaInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Moduloplataforma name.
   *
   * @return string
   *   Name of the Moduloplataforma.
   */
  public function getName();

  /**
   * Sets the Moduloplataforma name.
   *
   * @param string $name
   *   The Moduloplataforma name.
   *
   * @return \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface
   *   The called Moduloplataforma entity.
   */
  public function setName($name);

  /**
   * Gets the Moduloplataforma creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Moduloplataforma.
   */
  public function getCreatedTime();

  /**
   * Sets the Moduloplataforma creation timestamp.
   *
   * @param int $timestamp
   *   The Moduloplataforma creation timestamp.
   *
   * @return \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface
   *   The called Moduloplataforma entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Moduloplataforma published status indicator.
   *
   * Unpublished Moduloplataforma are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Moduloplataforma is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Moduloplataforma.
   *
   * @param bool $published
   *   TRUE to set this Moduloplataforma to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface
   *   The called Moduloplataforma entity.
   */
  public function setPublished($published);

  /**
   * Gets the Moduloplataforma revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Moduloplataforma revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface
   *   The called Moduloplataforma entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Moduloplataforma revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Moduloplataforma revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\smartnmsfrontend\Entity\moduloplataformaInterface
   *   The called Moduloplataforma entity.
   */
  public function setRevisionUserId($uid);

}
