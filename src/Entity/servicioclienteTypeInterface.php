<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Serviciocliente type entities.
 */
interface servicioclienteTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
