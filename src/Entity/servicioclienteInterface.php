<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Serviciocliente entities.
 *
 * @ingroup smartnmsfrontend
 */
interface servicioclienteInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Serviciocliente name.
   *
   * @return string
   *   Name of the Serviciocliente.
   */
  public function getName();

  /**
   * Sets the Serviciocliente name.
   *
   * @param string $name
   *   The Serviciocliente name.
   *
   * @return \Drupal\smartnmsfrontend\Entity\servicioclienteInterface
   *   The called Serviciocliente entity.
   */
  public function setName($name);

  /**
   * Gets the Serviciocliente creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Serviciocliente.
   */
  public function getCreatedTime();

  /**
   * Sets the Serviciocliente creation timestamp.
   *
   * @param int $timestamp
   *   The Serviciocliente creation timestamp.
   *
   * @return \Drupal\smartnmsfrontend\Entity\servicioclienteInterface
   *   The called Serviciocliente entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Serviciocliente published status indicator.
   *
   * Unpublished Serviciocliente are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Serviciocliente is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Serviciocliente.
   *
   * @param bool $published
   *   TRUE to set this Serviciocliente to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\smartnmsfrontend\Entity\servicioclienteInterface
   *   The called Serviciocliente entity.
   */
  public function setPublished($published);

  /**
   * Gets the Serviciocliente revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Serviciocliente revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\smartnmsfrontend\Entity\servicioclienteInterface
   *   The called Serviciocliente entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Serviciocliente revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Serviciocliente revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\smartnmsfrontend\Entity\servicioclienteInterface
   *   The called Serviciocliente entity.
   */
  public function setRevisionUserId($uid);

    /**
     * Gets the Serviciocliente revision author.
     *
     * @return \Drupal\zabbixentities\HostInterface
     *   The host entitis for the service.
     */
    public function getHosts();

}
