<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Serviciocliente entity.
 *
 * @ingroup smartnmsfrontend
 *
 * @ContentEntityType(
 *   id = "serviciocliente",
 *   label = @Translation("Customer Service"),
 *   bundle_label = @Translation("Type of Service"),
 *   handlers = {
 *     "storage" = "Drupal\smartnmsfrontend\servicioclienteStorage",
 *     "view_builder" = "Drupal\smartnmsfrontend\servicioclienteViewBuilder",
 *     "list_builder" = "Drupal\smartnmsfrontend\servicioclienteListBuilder",
 *     "views_data" = "Drupal\smartnmsfrontend\Entity\servicioclienteViewsData",
 *     "translation" = "Drupal\smartnmsfrontend\servicioclienteTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\smartnmsfrontend\Form\servicioclienteForm",
 *       "add" = "Drupal\smartnmsfrontend\Form\servicioclienteForm",
 *       "edit" = "Drupal\smartnmsfrontend\Form\servicioclienteForm",
 *       "delete" = "Drupal\smartnmsfrontend\Form\servicioclienteDeleteForm",
 *     },
 *     "access" = "Drupal\smartnmsfrontend\servicioclienteAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\smartnmsfrontend\servicioclienteHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "serviciocliente",
 *   data_table = "serviciocliente_field_data",
 *   revision_table = "serviciocliente_revision",
 *   revision_data_table = "serviciocliente_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer serviciocliente entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/smart/serviciocliente/{serviciocliente}",
 *     "add-page" = "/smart/serviciocliente/add",
 *     "add-form" = "/smart/serviciocliente/add/{serviciocliente_type}",
 *     "edit-form" = "/smart/serviciocliente/{serviciocliente}/edit",
 *     "delete-form" = "/smart/serviciocliente/{serviciocliente}/delete",
 *     "version-history" = "/smart/serviciocliente/{serviciocliente}/revisions",
 *     "revision" = "/smart/serviciocliente/{serviciocliente}/revisions/{serviciocliente_revision}/view",
 *     "revision_revert" = "/smart/serviciocliente/{serviciocliente}/revisions/{serviciocliente_revision}/revert",
 *     "translation_revert" = "/smart/serviciocliente/{serviciocliente}/revisions/{serviciocliente_revision}/revert/{langcode}",
 *     "revision_delete" = "/smart/serviciocliente/{serviciocliente}/revisions/{serviciocliente_revision}/delete",
 *     "collection" = "/smart/serviciocliente",
 *   },
 *   bundle_entity_type = "serviciocliente_type",
 *   field_ui_base_route = "entity.serviciocliente_type.edit_form"
 * )
 */
class serviciocliente extends RevisionableContentEntityBase implements servicioclienteInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the serviciocliente owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }
    /**
     * {@inheritdoc}
     */
    public function getDescription() {
        return $this->get('description')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description) {
        $this->set('description', $description);
        return $this;
    }
  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

    /**
     * {@inheritdoc}
     */
    public function getHosts() {
        return $this->get('host_id')->referencedEntities();
    }


    /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Serviciocliente entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Service.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

  $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The description of the Service.'))
      ->setRevisionable(TRUE)
      ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -4,
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

  $fields['host_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Host'))
      ->setDescription(t('Hosts that are part of the service.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'zabbixhost')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
          'weight' => 0,
      ])
      ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'weight' => 5,
          'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'placeholder' => '',
          ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Serviciocliente is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

    /**
     * Gets the Serviciocliente revision creation timestamp.
     *
     * @return int
     *   The UNIX timestamp of when this revision was created.
     */
    public function getRevisionCreationTime()
    {
        return $this->{static::getRevisionMetadataKey($this->getEntityType(), 'revision_created')}->value;
    }

    /**
     * Sets the Serviciocliente revision creation timestamp.
     *
     * @param int $timestamp
     *   The UNIX timestamp of when this revision was created.
     *
     * @return \Drupal\smartnmsfrontend\Entity\servicioclienteInterface
     *   The called Serviciocliente entity.
     */
    public function setRevisionCreationTime($timestamp)
    {
        $this->{static::getRevisionMetadataKey($this->getEntityType(), 'revision_created')}->value = $timestamp;
        return $this;
    }

    /**
     * Gets the Serviciocliente revision author.
     *
     * @return \Drupal\user\UserInterface
     *   The user entity for the revision author.
     */
    public function getRevisionUser()
    {
        return $this->{static::getRevisionMetadataKey($this->getEntityType(), 'revision_user')}->entity;
    }

    /**
     * Sets the Serviciocliente revision author.
     *
     * @param int $uid
     *   The user ID of the revision author.
     *
     * @return \Drupal\smartnmsfrontend\Entity\servicioclienteInterface
     *   The called Serviciocliente entity.
     */
    public function setRevisionUserId($uid)
    {
        $this->{static::getRevisionMetadataKey($this->getEntityType(), 'revision_user')}->target_id = $uid;
        return $this;
    }
}
