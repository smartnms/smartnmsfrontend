<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Serviciocliente type entity.
 *
 * @ConfigEntityType(
 *   id = "serviciocliente_type",
 *   label = @Translation("Serviciocliente type"),
 *   handlers = {
 *     "list_builder" = "Drupal\smartnmsfrontend\servicioclienteTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\smartnmsfrontend\Form\servicioclienteTypeForm",
 *       "edit" = "Drupal\smartnmsfrontend\Form\servicioclienteTypeForm",
 *       "delete" = "Drupal\smartnmsfrontend\Form\servicioclienteTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\smartnmsfrontend\servicioclienteTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "serviciocliente_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "serviciocliente",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/serviciocliente_type/{serviciocliente_type}",
 *     "add-form" = "/admin/structure/serviciocliente_type/add",
 *     "edit-form" = "/admin/structure/serviciocliente_type/{serviciocliente_type}/edit",
 *     "delete-form" = "/admin/structure/serviciocliente_type/{serviciocliente_type}/delete",
 *     "collection" = "/admin/structure/serviciocliente_type"
 *   }
 * )
 */
class servicioclienteType extends ConfigEntityBundleBase implements servicioclienteTypeInterface {

  /**
   * The Serviciocliente type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Serviciocliente type label.
   *
   * @var string
   */
  protected $label;

}
