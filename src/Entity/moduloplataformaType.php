<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Moduloplataforma type entity.
 *
 * @ConfigEntityType(
 *   id = "moduloplataforma_type",
 *   label = @Translation("Moduloplataforma type"),
 *   handlers = {
 *     "list_builder" = "Drupal\smartnmsfrontend\moduloplataformaTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\smartnmsfrontend\Form\moduloplataformaTypeForm",
 *       "edit" = "Drupal\smartnmsfrontend\Form\moduloplataformaTypeForm",
 *       "delete" = "Drupal\smartnmsfrontend\Form\moduloplataformaTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\smartnmsfrontend\moduloplataformaTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "moduloplataforma_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "moduloplataforma",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/moduloplataforma_type/{moduloplataforma_type}",
 *     "add-form" = "/admin/structure/moduloplataforma_type/add",
 *     "edit-form" = "/admin/structure/moduloplataforma_type/{moduloplataforma_type}/edit",
 *     "delete-form" = "/admin/structure/moduloplataforma_type/{moduloplataforma_type}/delete",
 *     "collection" = "/admin/structure/moduloplataforma_type"
 *   }
 * )
 */
class moduloplataformaType extends ConfigEntityBundleBase implements moduloplataformaTypeInterface {

  /**
   * The Moduloplataforma type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Moduloplataforma type label.
   *
   * @var string
   */
  protected $label;

}
