<?php

namespace Drupal\smartnmsfrontend\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Moduloplataforma type entities.
 */
interface moduloplataformaTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
