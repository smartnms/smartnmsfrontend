<?php

/**
 * @file
 * Contains modulesoverview.page.inc.
 *
 * Page callback for modulesoverview page.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for modulesoverview page.
 *
 * Default template: modulesoverview.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_modulesoverview(array &$variables) {

    foreach (Element::children($variables) as $key) {
        $variables['content'][$key] = $variables[$key];
    }

}

?>