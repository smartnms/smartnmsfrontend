<?php

/**
 * @file
 * Contains moduloplataforma.page.inc.
 *
 * Page callback for Moduloplataforma entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Moduloplataforma templates.
 *
 * Default template: moduloplataforma.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_moduloplataforma(array &$variables) {
  // Fetch moduloplataforma Entity Object.
  $moduloplataforma = $variables['elements']['#moduloplataforma'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

}
